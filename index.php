#!/usr/bin/php
<?php
	# include Class General, where extraction and validation of data takes place
	include('class/General.class.php');
	include('database.php');
	# include Class Database, where database function takes place
	include('class/Database.class.php');

	# initialize General Class
	$obj = new General;
	# initialize Database Class
	$db = new Database;

	# database connection
	$conn = $db->connect(DBSERVER_NAME,DBUSER,DBPASSWORD,DBNAME);
	# get argument and set to mode, Test mode perform everything the normal import does, but not insert the data into the database.
	$mode = count($argv) > 1 ? strtolower($argv[1]) == 'test' ? 0 : 1 : 1;

	# extract data from csv
	$data = $obj->extract_csv('file/stock.csv');

	/**I used array search and strtolower to find stock, cost in gbp and discontinued from header, so in case of column changes.*/
	# set index of stock
	$stock_index = array_search('stock', array_map('strtolower', $data[0]));
	# set index of cost
	$cost_index  = array_search('cost in gbp', array_map('strtolower', $data[0]));
	# set index of discontinued
	$discont_index  = array_search('discontinued', array_map('strtolower', $data[0]));

	# initialization of total process, success and skip
	$total_process = 0;
	$total_success = 0;
	$total_skip = 0;

	# Loop all rows starting index 1, where index 0 is the header
	foreach(array_slice($data,1) as $line):
		# counting process row
		$total_process = $total_process + 1;
		# get validated and fixed data and set in variable process
		$process = $obj->validate_line($obj->fix_line($line),$stock_index,$cost_index,$discont_index);
		# check if process status is success
		if($process['status'] == 'success'):
			# then count success
			$total_success = $total_success + 1;
			# get process data
			$process_data = $process['data'];
			# check mode; if mode is 'test', insertion of database is not applicable;
			if($mode):
				# insertion of data, send parameters connection and other data needed to insert in database
				$db->insert($conn,$process_data[1],$process_data[2],$process_data[0],date('Y-m-d'),$process_data[6]);
			endif;
		else:
			# count skip data
			$total_skip = $total_skip + 1;
		endif;
	endforeach;

	# echo reports of how many items were processed, how many were successful, and how many were skipped.
	echo 'Total Process: '.$total_process.PHP_EOL;
	echo 'Total Success: '.$total_success.PHP_EOL;
	echo 'Total Skip: '.$total_skip.PHP_EOL;

 ?>