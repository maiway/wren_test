<?php
 
class Database
{
	# this function is for databse connection using PDO
	public function connect($servername,$uname,$pword,$dbname)
	{
		try{
			# create connection
			$conn = new PDO('mysql:host='.$servername.';dbname='.$dbname,$uname,$pword);
			# set the PDO error mode to exeption
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
		}
		catch(PDOException $e)
		{
			# echo exception error message
			echo $e->getMessage();
		}
	}

	# this function is for insert row in database
	public function insert($conn,$product_name,$product_desc,$product_code,$added_date,$discont)
	{
		try{
			# set the prepared statement;
			$stmt = $conn->prepare('INSERT INTO tblProductData (strProductName,strProductDesc,strProductCode,dtmAdded,dtmDiscontinued) VALUES (:product_name, :product_desc, :product_code, :added_date, :discont)');
			# bind and execute
			$stmt->execute([
					':product_name' => $product_name,
					':product_desc' => $product_desc,
					':product_code' => $product_code,
					':added_date'   => $added_date,
					':discont' 	   => $discont
				]);
		}
		catch(Exception $e)
		{
			# echo exception error message
			echo $e->getMessage();
		}
	}
}

?>